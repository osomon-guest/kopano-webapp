#      Default NGinx server configuration for kopano-webapp-nginx.
# This file will be overridden by updates, keep your settings into the file
# '/etc/kopano/webapp/kopano-webapp-nginx-servers.conf'.

server {
    listen 80;
    root /usr/share/kopano-webapp;
    # Redirect all request to https
    rewrite ^(.*) https://$host$1 permanent;
    # No 'server_name' definition here, please add any server names into the
    # following included configuration.
    include /etc/kopano/webapp/kopano-webapp-nginx-servers.conf;
}

server {
    listen 443;
    # No 'server_name' definition here, please add any server names into the
    # following included configuration at the bottom of 'server {}'.
    root /usr/share/kopano-webapp;
    set $version @version@;

    ssl on;
    include /etc/kopano/webapp/kopano-webapp-nginx-ssl.conf;
    # Please note! You need to override the intermediate certificate and key
    # not here but within the included file
    # '/etc/kopano/webapp/kopano-webapp-nginx-servers.conf'

    # Pass PHP scripts to FastCGI server
    location ~ \.php$ {
        include snippets/fastcgi-php.conf;
        # With php-fpm (or other unix sockets):
        fastcgi_pass unix:/var/run/php/php$version-fpm.sock;
        # With php-cgi (or other tcp sockets):
        # fastcgi_pass 127.0.0.1:9000;
    }

     location / {
        root /usr/share/kopano-webapp;
        try_files $uri $uri/ =404;
        index index.php;
        location ~ \.php$ {
            include snippets/fastcgi-php.conf;
            # With php-fpm (or other unix sockets):
            fastcgi_pass unix:/var/run/php/php$version-fpm.sock;
            # With php-fpm (or other unix sockets):
            # fastcgi_pass 127.0.0.1:9000;
            fastcgi_param SCRIPT_FILENAME $request_filename;
        }
    }


    location ^~ /webapp {
        alias /usr/share/kopano-webapp;
        index index.php;
        location ~ \.php$ {
            include snippets/fastcgi-php.conf;

            fastcgi_pass unix:/var/run/php/php$version-fpm.sock;
            fastcgi_param SCRIPT_FILENAME $request_filename;
            # With php-fpm (or other unix sockets):
            # fastcgi_pass 127.0.0.1:9000;
        }
    }


    # Deny access to .htaccess files, if Apache's document root
    # concurs with nginx's one.
    location ~ /\.ht {
        deny all;
    }
    # Once again, add the needed values for 'server_name' in the included file
    # below.
    include /etc/kopano/webapp/kopano-webapp-nginx-servers.conf;
}

# vim: autoindent ts=4 sw=4 expandtab softtabstop=4 ft=conf
